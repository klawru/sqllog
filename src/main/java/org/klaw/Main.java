package org.klaw;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

@Command(name = "parse",
        header = "@|bold Siebel SQL Log Parser https://gitlab.com/Klawru/sqllog |@%n")
public class Main implements Callable<Integer> {
    @Option(names = {"-c", "--charset"}, defaultValue = "UTF-8", description = "UTF-8, US_ASCII, UTF_16, ... (default: ${DEFAULT-VALUE})")
    Charset charset;


    @Parameters(arity = "1..*", paramLabel = "FILE_PATH", description = "The files to parse")
    List<File> files;


    public static void main(String[] args) {
        int exitCode = new CommandLine(new Main()).execute(args);
        System.exit(exitCode);
    }

    private static String currentTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        return formatter.format(date);
    }

    private void parse(File sourceFile) throws IOException {
        Path sourcePath = sourceFile.toPath();
        Path tempPath = createTempFile(sourcePath.getParent(), sourceFile.getName());
        if (tempPath != null) {
            BufferedWriter writer = Files.newBufferedWriter(tempPath, charset);
            MyBufferedReader reader = createBufferedReader2(sourcePath, charset);
            new LogParser().parse(reader, writer);
            Files.move(tempPath, sourceFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
    }

    private MyBufferedReader createBufferedReader2(Path path, Charset cs) throws IOException {
        CharsetDecoder decoder = cs.newDecoder();
        InputStreamReader reader = new InputStreamReader(Files.newInputStream(path), decoder);
        return new MyBufferedReader(reader);
    }

    private static Path createTempFile(Path folder, String name) throws IOException {
        return Files.createTempFile(folder, name, "temp");
    }


    @Override
    public Integer call() throws Exception {
        for (File file : files) {
            System.out.println("Start parse '" + file + "' at " + currentTime());
            if (file.exists())
                parse(file);
            else
                System.err.println("File doesn't exist");
            System.out.println("End parse '" + file + "' at " + currentTime());
        }
        return 0;
    }
}
